django-haystack (3.3.0-2) unstable; urgency=medium

  * Team upload.
  * Remove dependency on python3-pkg-resources (Closes: #1083363)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 01 Feb 2025 01:52:52 +0100

django-haystack (3.3.0-1) unstable; urgency=low

  * New upstream version 3.3.0
  * Build using pybuild-plugin-pyproject and dh-sequence-python3.
  * Bump Standards-Version to 4.7.0.
  * Update year in d/copyright.
  * Depend on python3-all for autopkgtests.

 -- Michael Fladischer <fladi@debian.org>  Fri, 19 Jul 2024 08:30:29 +0000

django-haystack (3.2.1-1) unstable; urgency=low

  * New upstream release.
  * Use github tags instead of releases for d/watch.
  * Update year in d/copyright.
  * Bump Standards-Version to 4.6.0.1.
  * Use supported Python3 versions for import tests.

 -- Michael Fladischer <fladi@debian.org>  Tue, 21 Jun 2022 21:08:01 +0000

django-haystack (3.1.1-1) unstable; urgency=low

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-django-haystack-doc: Add Multi-Arch: foreign.

  [ Diego M. Rodriguez ]
  * d/tests: replace autodep8 with custom import test

  [ Michael Fladischer ]
  * New upstream release.
  * Update d/watch to work with github again.
  * Bump Standards-Version to 4.6.0.
  * Clean up haystack/version.py to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Tue, 31 Aug 2021 11:13:19 +0000

django-haystack (3.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Refer to common license file for Apache-2.0.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Michael Fladischer ]
  * New upstream release.
  * Set Rules-Requires-Root: no.
  * Bump debhelper version to 13.

 -- Michael Fladischer <fladi@debian.org>  Wed, 14 Oct 2020 13:44:04 +0200

django-haystack (2.8.1-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.
  * Bump standards version to 4.4.0 (no changes).

  [ Emmanuel Arias ]
  * Removed Python2 supported

 -- Emmanuel Arias <emmanuelarias30@gmail.com>  Wed, 24 Jul 2019 02:36:02 +0000

django-haystack (2.8.1-2) unstable; urgency=medium

  * Fix FTBFS by restoring dh_auto_clean.

 -- Michael Fladischer <fladi@debian.org>  Wed, 09 May 2018 22:14:27 +0200

django-haystack (2.8.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release.
  * d/control:
    - Add explicit dependency on python{,3}-pkg-resources for both python and
      python3 package. (Closes: #896263, #896296)
    - Bump Standards-Version to 4.1.4, no change required.
    - Remove X-Python fields
    - Clean django version dependency that is not relevant anymore
  * wrap-and-sort-ed.
  * d/rules:
    - dh_auto_clean deactivated as setup.py clean is a bit overkill here
    - Do not purge docs/_build in dh_clean rule as it's not providing any
      benefit.
    - Remove the useless include of pkg-info.mk
    - Add a comment to explain disabled tests
  * d/watch:
    - Bump version to 4 and use uscan tags
  * d/clean:
    - Remove the .pybuild directory

 -- Pierre-Elliott Bécue <becue@crans.org>  Tue, 08 May 2018 23:20:52 +0200

django-haystack (2.8.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Sat, 24 Mar 2018 19:40:40 +0100

django-haystack (2.7.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Michael Fladischer ]
  * New upstream release.
  * Always use pristine-tar.
  * Update license definition for SOLR auxiliary files.
  * Remove lintian override for missing GPG signature check on source
    tarball to serve as a reminder to improve on this.
  * Bump Standards-Version to 4.1.3.
  * Bump debhelper compatibility and version to 11.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Enable autopkgtest-pkg-python testsuite.
  * Build documentation in override_dh_sphinxdoc.

 -- Michael Fladischer <fladi@debian.org>  Wed, 14 Feb 2018 19:15:48 +0100

django-haystack (2.6.1-1) unstable; urgency=low

  * Upload to unstable.
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Jun 2017 21:26:15 +0200

django-haystack (2.6.1-1~exp1) experimental; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 31 May 2017 11:26:09 +0200

django-haystack (2.6.0-1~exp1) experimental; urgency=low

  * New upstream release.
  * Add .eggs/README.txt to d/clean to allow two builds in a row.
  * Add python(3)-setuptools-scm to Build-Depends as upstream started
    using it in their setup.py.
  * Remove unused lintian overrides.

 -- Michael Fladischer <fladi@debian.org>  Tue, 21 Mar 2017 12:40:47 +0100

django-haystack (2.5.1-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Tue, 03 Jan 2017 15:09:30 +0100

django-haystack (2.5.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * New upstream release (Closes: #828641).
  * Bump Standards-Version to 3.9.8.
  * Use github tarballs in d/watch.
  * Use python3-sphinx to build documentation.
  * Use https:// for copyright-format 1.0 URL.

 -- Michael Fladischer <fladi@debian.org>  Sun, 21 Aug 2016 12:35:16 +0200

django-haystack (2.4.1-1) unstable; urgency=low

  [ Michael Fladischer ]
  * New upstream release.

  [ SVN-Git Migration ]
  * git-dpm config
  * Update Vcs fields for git migration

 -- Michael Fladischer <fladi@debian.org>  Wed, 04 Nov 2015 13:11:02 +0100

django-haystack (2.4.0-2) unstable; urgency=low

  * Switch buildsystem to pybuild.
  * Add Python3 support through a separate package.
  * Add lintian override for missing upstream changelog.

 -- Michael Fladischer <fladi@debian.org>  Tue, 07 Jul 2015 22:12:20 +0200

django-haystack (2.4.0-1) unstable; urgency=low

  * New upstream release.
  * Remove files from d/copyright which are no longer shipped by
    upstream.
  * Use pypi.debian.net service for uscan.
  * Change my email address to fladi@debian.org.

 -- Michael Fladischer <fladi@debian.org>  Tue, 07 Jul 2015 16:18:03 +0200

django-haystack (2.3.1-1) unstable; urgency=medium

  * New upstream release (Closes: #755599).
  * Bump Standards-Version to 3.9.6.
  * Disable tests as they require a live SOLR and elasticsearch server.
  * Change file names for solr configuration files in d/copyright.
  * Make pysolr require at least version 3.2.0.
  * Add python-elasticsearch to Suggests.
  * Drop packages required by tests from Build-Depends:
    + python-django
    + python-httplib2
    + python-mock
    + python-pysolr
    + python-whoosh
  * Drop python-xapian from suggests as the xapian backend is not
    included.
  * Add django_haystack.egg-info/requires.txt to d/clean.
  * Remove empty lines at EOF for d/clean and d/rules.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Mon, 20 Oct 2014 14:18:24 +0200

django-haystack (2.1.0-1) unstable; urgency=low

  * Initial release (Closes: #563311).

 -- Michael Fladischer <FladischerMichael@fladi.at>  Thu, 13 Mar 2014 19:11:15 +0100
